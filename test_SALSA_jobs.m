function test_SALSA_jobs(jobname,varargin)
% TEST_SALSA_JOBS generates (multiple) problem settings for SALSA 
% (tensor completion) and lets SALSA.m solve it.
%
%   Input:
%       jobname --- string      (name in which results are saved)
%      varargin --- optional arguments
%
%   TEST_SALSA_JOBS(jobname,para_name1,para_value1,para_name2,para_value2,...)
%   tests SALSA for each combination between multiple values per parameter.
%
%   Each combination is tested para.sa times using random number generator
%   seeds 1,...,para.sa.
%
%   By the strings specifying parameters, each followed by its
%   proposed value(s), the fields of para are set (see examples).
%
%   The target tensor M is of dimension d and size n(1) x ... x n(d) and is
%   given through one of three ways (M is never explicitely calculated)
%   depending on para.tensortype:
%
%       fct [function]:         M(i_1,...,i_d) = para.f([i_1,...,i_d]) 
%                               for a function f.
%                               This changes certain default values
%                               automatrically if para.use_fct_default = 1.
%                               (see get_f.m)
%       rnd [exact low rank]:   M is a tensor represented by tilde_G of
%                               rank r_mu <= para.r_rnd, where tilde_G is 
%                               generated randomly.
%                               (see init_by_random_tensor.m)
%       spc [special tensor]:   M is the rank adaption test tensor for
%                               k = para.r_rnd .
%                               (see init_by_special_tensor.m)
%
%   All parameters (other than for SALSA) are:
%
%   tensor M:
%
%                 d --- int(>2)         (dimension of M)
%                 n --- 1 x d int       (tensor modes sizes: n(1) x ... x n(d))
%        tensortype --- string          ('fct', 'rnd' or 'spc', see above)
%   'fct':
%            f_name --- string          (Specifies the function used for M.
%                                        See get_f.m)                                                                                  
%   use_fct_default --- bool            (1: use functions default parameters)  
%
%   'rnd'/'spc':
%             r_rnd --- int             (maximal rank of random tensor)
%
%   sampling 
%                 c --- int             (sampling factor)
%        r_fin_samp --- int             (assumed sampling rank)
%
%   testing:
%             sa --- int        (number of times each test is performed)
%      rand_seed --- int        (random seed, 0: seed according to trial 
%                                number)
%      rec_limit --- double     (relative residual required to regard 
%                                a reconstruction test as successful)
%         c_skip --- bool       (skip if same test with lower c was
%                                already perfect)
%
%   The sampling size matches p = c*r_fin_samp*r_fin_samp*sum(n). 
%   The sampling set P and test set C are of length (at most)
%   c*r_fin_samp*r_fin_samp*sum(n),
%   where c is regarded as sampling factor. These are generated
%   quasi-randomly ensuring that no slice density is less than
%   c*r_fin_samp*r_fin_samp (usually the slice density is far above this
%   value anyway even for completely random sampling).
%   (see create_sampling)
%
%   As first entry in varargin, one may enter 'overwrite', which will cause
%   the program to overwrite existing results with name SALSA_<jobname>.mat
%
%   All other entries come in pairs of either
%
%   (1)  <para_name>, <para_value>
%   (2)  {<para_name1>,<para_name2>,...}, {<para_value1>,<para_value2>,...}
%       
%   where <para_name> is a string and <para_value> is either an int, double,
%   string, function handle or a cell containing multiple of one type.
%   If para.(<para_name>) is not a string, but <para_value> is,
%   then the string will be evaluated as code using the 'eval' command.
%   If <para_value> is a single cell as in (1), the program will run through
%   all combinations between this and all other cell inputs.
%
%   In case (2), only the j-th component of each <para_valuei> is combined
%   to a parameter setting (see examples).
%
%   There are relative parameters which rely on other parameters if not 
%   further specified by the input.
%
%   For the meaning of parameters required for SALSA.m, see the 
%   documentation in that file.
%
%   A maximal rank r_lim has to be provided for SALSA. As default value, a 
%   rank slightly higher than the sampling rank r_fin_samp is chosen, i.e.
%   para.r_lim = para.r_fin_samp + para.r_fin_add.
%
%   The control set P_2 is split of P s.t. |P_2|/|P| = para.p2_ratio.
%
%   TEST_SALSA_JOBS requires other programs:
%
%     -> init_by_random_tensor.m
%     -> init_by_special_tensor.m
%     -> create_sampling.m
%     -> get_f.m
%     -> gen_functions_defaults.m
%
%   Examples:
%
%       test_SALSA_jobs('test','overwrite');
%       % will run 20 times the test specified by the default values and 
%       % overwrite the file SALSA_test.m, should it already exist.
%
%       test_SALSA_jobs('test','overwrite',...
%           {'d', 'r_fin_samp'}, {{4,5}, {6,10}});
%       % will run 2 tests, the first with d = 4, r_fin_samp = 6, the 
%       % second one with d = 5, r_fin_samp = 10.
%
%       test_SALSA_jobs('test',...
%           'sa',2,...
%           'd',4,...
%           'c',5,...
%           'tensortype', 'rnd', ...
%           'r_rnd', {4,5}, ...
%           'n', {'8*ones(1,para.d)', '10*ones(1,para.d)'},...
%           'show_sing_spec',5);
%       % will run 4 tests, each 2 times, constructing a random tensor of 
%       % maximal rank 4 or 5 and n = 8 or n = 10. If the file
%       % SALSA_test.mat already exists, the program will refuse to continue.
%
%   Output:
%   
%   All results will be saved in SALSA_<jobname>.mat:
%
%     PARA --- number_of_tests x 1 cell of type struct
%         (PARA{i} equals the para used for test i)
%     INFO --- number_of_tests x para.sa cell of type struct
%         (INFO{i}{j} is the output of SALSA for test nr. i and trial nr. j
%         j = 1,...,para.sa)
%   result --- number_of_tests x 1 cell of type para.sa x 1 doubles
%         (separatly stores output information, that is
%         .c(:)  --- relative residuals of chosen representations on C
%         .p(:)  --- relative residuals of chosen representations on P
%         .p2(:) --- relative residuals of chosen representations on P2
%         .t(:)  --- required cpu time
%         .i(:)  --- required iterations)
%
%   e.g., result{i}.c(j) is the relative residual in test i, trial j, in
%   which parameters para = PARA{i} and the random seed j have been used,
%   if para.rand_seed == 0.
%
%   Additionally, the original files SALSA.m and test_SALSA_jobs as text 
%   files as well as the input varargin is saved.
%
%   Author:
%
%   Written by Sebastian Kraemer, IGPM RWTH Aachen University (2019)
%   In case of problems or questions, feel free to send an email to
%   kraemer@igpm.rwth-aachen.de
%
%   See the license file for copyright issues.
%
%   See also: SALSA, BENCHMARK, GET_F, GEN_FUNCTIONS_DEFAULTS,
%   INIT_BY_RANDOM_TENSOR, INIT_BY_SPECIAL_TENSOR, CREATE_SAMPLING
%

algorithm_name = 'SALSA';

%% ------------------------------------------------------------------------
para = struct;
para.f = @(x) 0; % No not change this. It will be overwritten.
%% Default Parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%% %% Problem Parameters: %% %%

%% Target tensor:
para.d = 6;                  
para.n = 12*ones(1,para.d); 
para.use_fct_default = 1;    
para.tensortype = 'fct';   
para.f_name = 'domino';     
para.r_rnd = 6;          

%% Sampling:
para.c = 2;               
para.r_fin_samp = 6;       

%% Testing:
para.sa = 20;             
para.rand_seed = 0;  
para.c_skip = 0;           
para.rec_limit = 0;      

%% %% SALSA Default Parameters (see SALSA.m) %% %%
%
%% Printing and plotting 
para.verbose = true; % (can cause considerable slowdown)           
per = 50;
para.control = per;         
para.print_info = per;      
para.show_sing_spec = 0;    

%% r_lim:
para.r_fin_add = 1;         
para.r_lim = para.r_fin_samp + para.r_fin_add;

%% Secondary (best not to change these)

% Validation set:
para.p2_ratio = 1/20;        

% Rank adaption:
para.r_uni_start = 1;        
para.omega_0 = 0.5;           
para.f_omega_start = 1.02;
para.f_omega_min = 1.0005;
para.f_omega_max = 1.1;
para.num_minor_sing_vals = 2;
para.sigma_min_fac = 1/10;
para.rate_of_progress = 0.005; 

% CG modifaction:
para.cg = 1;                 
para.cg_break_fac = 0.01;     

% Stopping criteria:
para.iter_min = 10; 
para.maxit = 10000;          
para.res_min = 1e-10;        
para.res_p2_max = 5;        
para.omega_sigma_min_fac = 1/50;

% -------------------------------------------------------------------------
%% From here automatically...
%
%
%
%%
%%
%%

%% Parameter system
FLIB = gen_functions_defaults();
para = set_para_to_default(para,para);
para = set_function_default(FLIB,para);
para = get_f(para);
para = set_relative_parameter(para);
para_default = para;
% -------------------------------------------------------------------------

%% Save codes
fid = fopen([algorithm_name,'.m'],'rt');
version = textscan(fid,'%s','Delimiter','\n');
fclose(fid);

fid = fopen(['test_',algorithm_name,'_jobs.m'],'rt');
version_test = textscan(fid,'%s','Delimiter','\n');
fclose(fid);

%% Possibly allow to overwrite old test with same name
ovw = 0;
if nargin > 1 && isequal(varargin{1},'overwrite')
    ovw = 1; % allow to overwrite an existing file
end

%% Check and transfer input
nargin_ = nargin - 1 - ovw;
if mod(nargin_,2)~=0
    error('Wrong number of arguments \n');
end

arglength = ones(nargin_/2,1);
argval = cell(nargin_/2,1);
for i = 1:2:nargin_
    if isa(varargin{i+1+ovw},'cell')
        argval{(i+1)/2} = varargin{i+1+ovw};
    else
        argval{(i+1)/2} = {varargin{i+1+ovw}};
    end
    arglength((i+1)/2) = length(argval{(i+1)/2});
end

I = ones(1,nargin_/2); I(1) = 0;
test_nr = 0;
number_of_tests = prod(arglength);

%% check if test name already exists
if ~ovw && exist([algorithm_name,'_',jobname,'.mat'], 'file') == 2
    error('Will not overwrite existing files. Delete earlier results manually or use the overwrite option.');
end

%% Start test series
result = cell(number_of_tests);

%% Run through all parameter combinations
% parameters in each test
PARA = cell(number_of_tests);

% info output of each test
INFO = cell(number_of_tests);

save([algorithm_name,'_',jobname],'PARA','INFO','result','varargin','nargin_','version','version_test','arglength','argval');
perfect_rec = zeros(number_of_tests,1);
while test_nr < number_of_tests
    test_nr = test_nr + 1;
    fprintf('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n');
    fprintf('Starting test series %d of %d (%s): \n',test_nr,number_of_tests,[algorithm_name,'_',jobname]);
    
    para = set_para_to_default(para,para_default);
    
    I(1) = I(1) + 1;
    for j = 1:nargin_/2
        if I(j) > arglength(j)
            I(j) = 1;
            I(j+1) = I(j+1) + 1;
        end
        
        % Set current parameter combination for I(j)
        para = set_para(FLIB,varargin{2*j-1+ovw},argval{j}{I(j)},para);
    end
    % Get function again
    if strcmp(para.tensortype,'fct')
        para = set_function_default(FLIB,para);
        para = get_f(para);
    end
    para = set_relative_parameter(para);
    
    result{test_nr}.c = zeros(1,para.sa);   % res_c
    result{test_nr}.p2 = zeros(1,para.sa);  % res_p2
    result{test_nr}.p = zeros(1,para.sa);   % res_p
    result{test_nr}.t = zeros(1,para.sa);   % time
    result{test_nr}.i = zeros(1,para.sa);   % iterations
    PARA{test_nr} = para;
    INFO{test_nr} = cell(1,para.sa);
    
    % skip already perfect test
    skip = 0;
    if para.c_skip
        para_ = para;
        for i = 1:test_nr-1
            para_.c = PARA{i}.c;
            if isequal(para_,PARA{i}) && para.c > PARA{i}.c && perfect_rec(i)
                skip = i;
                break;
            end
        end
    end
    
    if skip
        fprintf('\n [!] Skipping test_nr %d with c = %d, since test_nr %d with c = %d was perfect \n\n',test_nr,para.c,skip,PARA{skip}.c);
        save([algorithm_name,'_',jobname],'PARA','INFO','result','varargin','nargin_','version','version_test','arglength','argval');
    else
        
        % start trials
        display(para);
        for trial = 1:para.sa
            % set/proceed random seed
            if para.rand_seed == 0
                rand_seed = trial;
            else
                rand_seed = para.rand_seed;
            end
            rng(rand_seed);
                                                        
            % check n and d
            if numel(para.n)==1
                fprintf('Using uniform n = %d \n',para.n);
                para.n = para.n*ones(1,para.d);
            end
            
            % generate training and test set          
            [P,C] = create_sampling(para.c,para.r_fin_samp,para.n,para.d);
            
            % split off validation set P2 from P
            [P,P2] = split_off_P2(P);
            
            % calculate values of target tensor M
            switch para.tensortype
                case 'fct'
                    M_P = para.f(P);
                    M_P2 = para.f(P2);
                    M_C = para.f(C);
                case 'rnd'
                    [M_P,M_P2,M_C,r] = init_by_random_tensor(rand_seed,para,P,P2,C);
                    % (the rng is again set to rand_seed in this code)
                    fprintf('%d ',r);
                    fprintf('\n');
                case 'spc'
                    [M_P,M_P2,M_C,para] = init_by_special_tensor(para,P,P2,C);
            end
            
            % summarize problem setting
            W = struct(...
                'P',P,...
                'C',C,...
                'P2',P2,...
                'M_P',M_P,...
                'M_C',M_C,...
                'M_P2',M_P2); %#ok<NASGU>
            
            % SOLVE PROBLEM SETTING
            fprintf('----------------------------------------------------- \n');
            fprintf('Trial nr.: %d \n',trial);
            fprintf('p: %d, p2: %d, pc: %d \n\n',size(P,1),size(P2,1),size(C,1));
            
            [~,~,info] = eval([algorithm_name,'(para.d,para.n,W,para)']);

            fprintf('----------------------------------------------------- \n');
            
            % save results
            result{test_nr}.c(trial) = info.res_c_ch/norm(M_C);
            result{test_nr}.p(trial) = info.res_p_ch/norm(M_P);
            result{test_nr}.p2(trial) = info.res_p2_ch/norm(M_P2);
            result{test_nr}.t(trial) = info.time;
            result{test_nr}.i(trial) = info.iter;
            INFO{test_nr}{trial} = info;
            save([algorithm_name,'_',jobname],'PARA','INFO','result','varargin','nargin_','version','version_test','arglength','argval');
        end
        fprintf('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n');
        
        % count successful recoveries (if relevant)
        sucs = sum(result{test_nr}.c(:)<para.rec_limit);
        if sucs == para.sa
            perfect_rec(test_nr) = 1;
        end
    end
end

    function [P,P2,p2] = split_off_P2(P)
        P_ = length(P);
        p2 = floor(para.p2_ratio*P_);
        temp = randperm(P_);
        P2 = P(temp(1:p2),:);
        P = P(temp(p2+1:end),:);
        p2 = length(P2);
    end

end
function para = set_para_to_default(para,para_default)
% Sets parameters back to original default values
fields = fieldnames(para);

for i = 1:length(fields)
    para.(fields{i}) = para_default.(fields{i});
    para.set.(fields{i}) = 0;
end

end

function para = set_function_default(FLIB,para)
% Sets all parameters which are not modified by input to function specific
% defaults
fields = fieldnames(para);

if ~isfield(FLIB,para.f_name)
    error('function does not exist (f_name = %s)',f_name);
end

for i = 1:length(fields)
    if ~isequal(fields{i},'set') && ~para.set.(fields{i}) && isfield(FLIB.(para.f_name),fields{i})
        if isa(para.(fields{i}),'double') && isa(FLIB.(para.f_name).(fields{i}),'char')
            para.(fields{i}) = eval(FLIB.(para.f_name).(fields{i}));
        else
            para.(fields{i}) = FLIB.(para.f_name).(fields{i});
        end
    end
end

end

function para = set_para(FLIB,A1,A2,para)
% cell <-> cell (assign multiple parameters at once)
if isa(A1,'cell')
    if ~isa(A2,'cell') || length(A2)~=length(A1)
        error('Wrong arguments \n');
    end
    for ii = 1:length(A1)
        para = set_para(FLIB,A1{ii},A2{ii},para);
    end
else
    % Check if field exists in para
    if ~isfield(para,A1)
        error('Parameter %s does not exist \n',A1);
    end
    
    % double <-> double
    if isa(para.(A1),'double') && isa(A2,'double')
        para.(A1) = A2;
        fprintf(' > para.%s <-- %s \n',A1,num2str(A2));
        
        % char <-> char
    elseif isa(para.(A1),'char') && isa(A2,'char')
        para.(A1) = A2;
        fprintf(' > para.%s <-- %s \n',A1,A2);
        
        % not char <-> char (assign as written in char with current values)
    elseif ~isa(para.(A1),'char') && isa(A2,'char')
        para.(A1) = eval(A2);
        fprintf(' > para.%s <-- %s = %s \n',A1,A2,num2str(para.(A1)));
        
        % function handle <-> function handle
    elseif isa(para.(A1),'function_handle') && isa(A2,'function_handle')
        para.(A1) = A2;
        fprintf(' > para.%s <-- %s \n',A1,func2str(para.(A1)));
        
        % otherwise
    else
        class(para.(A1))
        class(A2)
        error('Wrong types \n');
    end
    
    para.set.(A1) = true;
    
    if strcmp(A1,'f_name') && strcmp(para.tensortype,'fct')
        para = set_function_default(FLIB,para);
    end
    
end

end

function para = set_relative_parameter(para)
% Sets relative parameter if not modified through input

% r_lim:
if ~para.set.r_lim
    para.r_lim = para.r_fin_samp + para.r_fin_add;
    fprintf(' >> para.r_lim <-- %s \n',num2str(para.r_lim));
end

end

