%% This is the simplest example mentioned in SALSA.m:
%
%
%% You need to download multiprod.m first.
% https://www.mathworks.com/matlabcentral/fileexchange/8773-multiple-matrix-multiplications-with-array-expansion-enabled
%

f = @(X) sum(X,2).^2; % is exactly rank 3

d = 5;
n_uni = 10;
n = n_uni*ones(1,d);

p = 1000;  % smapling size
p2 = 50;   % validation size
pc = 1000; % test size

W.P = randi(n_uni,p,d);
W.M_P = f(W.P);

W.P2 = randi(n_uni,p2,d);
W.M_P2 = f(W.P2);

W.C = randi(n_uni,pc,d);
W.M_C = f(W.C);

para = struct;
para.print_info = 10; % print every 10 iterations
para.show_sing_spec = 5; % plot singular values every 5 iterations

SALSA(d,n,W,para);