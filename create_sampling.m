function [P,C] = create_sampling(c,k,n,d)
% CREATE_SAMPLING(c,k,n,d) creates quasi-random samplings sets of size 
% c*k*k*sum(n) x d ensuring a certain slice density
%
%   Author:
%
%   Written by Sebastian Kraemer, IGPM RWTH Aachen University (2019)
%   In case of problems or questions, feel free to send an email to
%   kraemer@igpm.rwth-aachen.de
%
%   See the license file for copyright issues.

P = rand_P_non_uni(c,k,n,d);
C = rand_P_non_uni(c,k,n,d);

end

function P = rand_P_non_uni(c,k,n,d)
p = c*k*k*sum(n);
m = c*k*k;

P = zeros(p,d);
offset = 0;

for s = 1:d
    P(:,s) = floor(n(s)*rand(p,1))+1;
    v = repmat(1:n(s),m,1);
    P(offset + (1:m*n(s)),s) = v(:);
    offset = offset + m*n(s);
end

P = unique(P,'rows');

end