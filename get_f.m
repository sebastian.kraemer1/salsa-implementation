function para = get_f(para)
% GET_F provides a function according to para.f_name
%
%   Author:
%
%   Written by Sebastian Kraemer, IGPM RWTH Aachen University (2019)
%   In case of problems or questions, feel free to send an email to
%   kraemer@igpm.rwth-aachen.de
%
%   See the license file for copyright issues.

d = para.d;
f = para.f;

if strcmp(para.f_name,'generic')
    per = 1:8;
    f =  @(X) sin(X(:,per(1))+X(:,per(2))).*cos(X(:,per(3))+X(:,per(4))./X(:,per(5)))./(X(:,per(6))+X(:,per(7)).*X(:,per(8)));
end
if strcmp(para.f_name,'generic2')
    f =  @(X) (1/4*X(:,1).*cos(X(:,3)-X(:,8)) + X(:,2).^2./(X(:,6)+X(:,7)+X(:,1)) + X(:,5).^(3).*sin((X(:,4)+X(:,3))));
end
if strcmp(para.f_name,'generic3')
    f =  @(X) (X(:,4)./(X(:,2)+X(:,6)) + X(:,1)+X(:,3)-X(:,5)-X(:,7)).^2;
end
if strcmp(para.f_name,'generic4')
    f =  @(X) sqrt(X(:,3)+X(:,2) + 1./10*(X(:,8)+X(:,7)+X(:,4)+X(:,5)+X(:,9)) + 1./20*(X(:,11) + X(:,1) - X(:,10) - X(:,6)).^2);
end
if strcmp(para.f_name,'sin1')
    f =  @(X) (sin(sum(X(:,1:5)/5,2))-sin(sum(X(:,6:8)/3,2))); % rank 3
end
if strcmp(para.f_name,'sin2')
    f =  @(X) (sin(sum(X(:,1:5)/5,2))-sin(sum(X(:,6:8)/3,2))).^2; % rank 5
end
if strcmp(para.f_name,'sin3')
    f =  @(X) (sin(sum(X(:,1:5)/5,2))-sin(sum(X(:,6:8)/3,2))).^3; % rank 7
end
if strcmp(para.f_name,'sin3_rev')
    f =  @(X) (sin(sum(X(:,1:3)/3,2))-sin(sum(X(:,4:8)/5,2))).^3; % rank 7
end
if strcmp(para.f_name,'domino')
    f =  @(X) (1./(1+sum(X(:,(1:d-1))./(X(:,2:d)),2)));
end
if strcmp(para.f_name,'domino_2knots')
    per = 1:d;  %randperm(d)
    f =  @(X) (1./(1+sum(X(:,per(1:floor(d/3)-1))./(X(:,per(2:floor(d/3)))),2))).*...
        (1./(1+sum(X(:,per(floor(d/3)+1:floor(2*d/3)-1))./(X(:,per(floor(d/3)+2:floor(2*d/3)))),2))).*...
        (1./(1+sum(X(:,per(floor(2*d/3)+1:end-1))./(X(:,per(floor(2*d/3)+2:end))),2)));
end
if strcmp(para.f_name,'rev_domino_2knots')
    f =  @(X) (1./(1+sum((X(:,2:floor(d/3)))./X(:,(1:floor(d/3)-1)),2))).*...
        (1./(1+sum((X(:,floor(d/3)+2:floor(2*d/3)))./X(:,(floor(d/3)+1:floor(2*d/3)-1)),2))).*...
        (1./(1+sum((X(:,floor(2*d/3)+2:end))./X(:,(floor(2*d/3)+1:end-1)),2)));
end
if strcmp(para.f_name,'one_over_norm_2knots')
    f =  @(X) 1./sqrt(sum(X(:,1:floor(d/2)).*X(:,1:floor(d/2)),2)).*1./sqrt(sum(X(:,floor(d/2)+1:end).*X(:,floor(d/2)+1:end),2));
end
if strcmp(para.f_name,'one_over_norm')
    f =  @(X) 1./sqrt(sum(X(:,:).*X(:,:),2));
end

para.f = f;

end