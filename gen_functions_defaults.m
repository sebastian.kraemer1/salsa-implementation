function FLIB = gen_functions_defaults()
% GEN_FUNCTIONS_DEFAULTS provides default values for each function
%
%    Author:
%
%   Written by Sebastian Kraemer, IGPM RWTH Aachen University (2019)
%   In case of problems or questions, feel free to send an email to
%   kraemer@igpm.rwth-aachen.de
%
%   See the license file for copyright issues.

FLIB = struct;

%% Generic function
FLIB.generic.d = 8;
FLIB.generic.n = '8*ones(1,para.d)';
FLIB.generic.r_fin_samp = 6;
FLIB.generic.c = 4;

%% Generic function
FLIB.generic2.d = 8;
FLIB.generic2.n = '8*ones(1,para.d)';
FLIB.generic2.r_fin_samp = 6;
FLIB.generic2.c = 4;

%% Generic function
FLIB.generic3.d = 7;
FLIB.generic3.n = '8*ones(1,para.d)';
FLIB.generic3.r_fin_samp = 6;
FLIB.generic3.c = 4;

%% Generic function
FLIB.generic4.d = 11;
FLIB.generic4.n = '8*ones(1,para.d)';
FLIB.generic4.r_fin_samp = 6;
FLIB.generic4.c = 4;

%% Sin function
FLIB.sin1.d = 8;
FLIB.sin1.n = '8*ones(1,para.d)';
FLIB.sin1.r_fin_samp = 3;
FLIB.sin1.c = 2;

%% 
FLIB.sin2.d = 8;
FLIB.sin2.n = '8*ones(1,para.d)';
FLIB.sin2.r_fin_samp = 5;
FLIB.sin2.c = 2;

%% 
FLIB.sin3.d = 8;
FLIB.sin3.n = '8*ones(1,para.d)';
FLIB.sin3.r_fin_samp = 7;
FLIB.sin3.c = 2;

%% 
FLIB.sin3_rev.d = 8;
FLIB.sin3_rev.n = '8*ones(1,para.d)';
FLIB.sin3_rev.r_fin_samp = 7;
FLIB.sin3_rev.c = 2;

%% Domino tensor with two knots
FLIB.domino_2knots.d = 9;
FLIB.domino_2knots.n = '12*ones(1,para.d)';
FLIB.domino_2knots.r_fin_samp = 6;
FLIB.domino_2knots.c = 2;

%% Domino tensor with two knots with reverse modes
FLIB.rev_domino_2knots.d = 9;
FLIB.rev_domino_2knots.n = '12*ones(1,para.d)';
FLIB.rev_domino_2knots.r_fin_samp = 6;
FLIB.rev_domino_2knots.c = 2;

%% Domino tensor
FLIB.domino.d = 9;
FLIB.domino.n = '12*ones(1,para.d)';
FLIB.domino.r_fin_samp = 6;
FLIB.domino.c = 2;

%% One over norm tensor with two knots
FLIB.one_over_norm_2knots.d = 9;
FLIB.one_over_norm_2knots.n = '10*ones(1,para.d)';
FLIB.one_over_norm_2knots.r_fin_samp = 4;
FLIB.one_over_norm_2knots.c = 5;

%% One over norm tensor
FLIB.one_over_norm.d = 9;
FLIB.one_over_norm.n = '10*ones(1,para.d)';
FLIB.one_over_norm.r_fin_samp = 4;
FLIB.one_over_norm.c = 5;
end