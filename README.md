--- --- --- 

For software copyright issues, see the LICENSE file.

Please note the copyright and citation issues implied by the publication and preprint versions of the related paper
"Stable ALS Approximation in the TT-Format for Rank-Adaptive Tensor Completion" by Lars Grasedyck and Sebastian Kraemer

--- --- ---

HOW TO GET STARTED:

First download 'multiprod.m' written by Paolo de Leva (available on mathworks.com) for faster multiplication of arrays of matrices:
https://www.mathworks.com/matlabcentral/fileexchange/8773-multiple-matrix-multiplications-with-array-expansion-enabled

SALSA.m and test_SALSA_jobs.m include comprehensive documentations as well as simple examples to run.
benchmark.m can run the SALSA tests presented in the paper.
example.m contains the simplest example mentioned in SALSA.m

