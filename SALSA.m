function [G_ch,r_ch,info] = SALSA(d,n,W,para)
% SALSA  [S]table [A]lternating [L]east [S]quares [A]pproximation for tensor
% completion in the tensor train (TT) format
%
%   [G_ch,r_ch] = SALSA(d,n,W,para) performs a rank adaptive completion:
%
%   Input:
%
%          d --- int(>2)         (dimension/order of tensor)
%          n --- 1 x d int       (tensor modes sizes: n(1) x ... x n(d))
%
%        W.P --- p x d int       (coordinates of sampling points/training set)
%      W.M_P --- p x 1 double    (values at sampling points/training set)
%
%       W.P2 --- p2 x d int      (coordinates of validation points)
%     W.M_P2 --- p2 x 1 double   (values at validation points)
%
%       para --- struct          (parameters, see below)
%
%   Optional:
%
%        W.C --- pc x d int      (coordinates of test points)
%      W.M_C --- pc x 1 double   (values at test points)
%
%   Output:
%
%       G_ch --- 1 x d cell      (chosen representation)
%       r_ch --- 1 x d+1 int     (rank of G_ch)
%
%   While G is the iterate representing the tensor A which is optimized by
%   minimizing the least squares difference on the samplings set
%       res_p = norm(A_P - M_P)  ->  min
%   G_ch is the representation of the one iterate A_ch which minimizes/ed:
%       res_p2 = norm(A_ch)_P2 - M_P2)  ->  min
%   and hence mostly not the last iterate G:
%
%   | iter increases as ALS optimization on training set proceeds:
%   v
%      G = G_0 (initial representation of constant rank 1 tensor)
%      :
%      G = G_ch (previous iterate which minimized res_p2, with rank r_ch) 
%      :
%      G (iterate minimizing res_p, with rank r)
%
%   Due to the (adaptive) low rank constraint, it is assumed that hence 
%   M is recovered by A_ch.
%   
%   The optional input W.C and W.M_C is treated as unknown and only used
%   for print output (res_c = norm(A_C - M_C)).
%
%   In d = 2, one could use for example sparse(P(:,1),P(:,2),M_P) to construct
%   the available part of M. For d > 2, the analogon would be
%   sparse(P(:,1),P(:,2),P(:,3),...,M_P) (which is however not implemented in
%   Matlab).
%
%   During the optimization, each cell entry G{s} of G (analogously G_ch) 
%   is an r_lim x r_lim x n(s) double. The relevant part has to be accessed
%   with G{s}(1:r(s),1:r(s+1),:). The output G (and G_ch) however is 
%   reduced to the this relevant part.
%
%   SALSA requires other programs:
%
%       -> 'multiprod.m' written by Paolo de Leva (available on
%       mathworks.com) for faster multiplication of arrays of matrices.
%
%   Tuning parameters or options can be specified in para. The default
%   parameters are given below in the code.
%
%   Only r_lim and verbose parameters should be changed.
%
%   Documentation (verbose and intervals):
%
%            verbose --- bool      (save/print/plot additional information)
%            control --- int       (evaluating res_c / 0: do not at all)
%         print_info --- int       (print information / 0: do not at all))
%     show_sing_spec --- int       (plot sing vals / 0: do not at all))
%              movie --- bool      (record movie of singular value plot)
%
%   Rank adaption:
%
%                   r_lim --- int         (rank limit)
%             r_uni_start --- int         (initial rank)
%                 omega_0 --- double(<1)  (for initialization of omega)
%           f_omega_start --- double(>1)  (initial decline factor of omega)
%             f_omega_min --- double(>1)  (minimal allowed factor)
%             f_omega_max --- double(>1)  (maximal allowed factor)
%     num_minor_sing_vals --- int         (number of minor singular values)
%           sigma_min_fac --- double(<1)  (factor for sigma_min)
%        rate_of_progress --- double(<1)  (desired rate of progress)
%
%   Stopping criteria:
%
%       iter_min --- int         (minimum of performed iterations)
%          maxit --- int         (maximal number of iterations)
%        res_min --- double(~=0) (algorithm breaks if residual is lower)
%     res_p2_max --- double(>1)  (will stop if res_p2>res_p2_ch*res_p2_max)
%  omega_..._fac --- double(<1)  (used to determine if omega is too low)
%
%   Coarse CG:
%
%               cg --- bool        (specifies if (1) cg is used or (0) not)
%     cg_break_fac --- int(<1)     (tolerance used in each cg iteration)
%
%
%   [G_ch,r_ch,info] = SALSA(d,n,W,para) returns additional information in
%   the struct info with fields:
%
%         r --- 1 x d+1 int     (exact rank of recovery)
%         G --- 1 x d struct    (last iterate)
%       A_C --- pc x 1 double   (recovered values on test set by G)
%
%     res_c --- double      (residual on test set)
%     res_p --- double      (residual on sampling set)
%    res_p2 --- double      (residual on validation set)
%
%         + all previous fields of info in a "_ch" version for G_ch
%
%      time --- double      (passed cpu time until last iterate)
%      iter --- int         (number of total iterations)
%
%   Example:
%
%       f = @(X) sum(X,2).^2; % is exactly rank 3
%   
%       d = 5;
%       n_uni = 10;
%       n = n_uni*ones(1,d);
%       
%       p = 1000;  % smapling size
%       p2 = 50;   % validation size
%       pc = 1000; % test size
%       
%       W.P = randi(n_uni,p,d);
%       W.M_P = f(W.P);
%       
%       W.P2 = randi(n_uni,p2,d);
%       W.M_P2 = f(W.P2);
%       
%       W.C = randi(n_uni,pc,d);
%       W.M_C = f(W.C);
%       
%       para = struct;
%       para.print_info = 10; % print every 10 iterations
%       para.show_sing_spec = 5; % plot singular values every 5 iterations
%       
%       SALSA(d,n,W,para);
%
%   Longer example:
%
%       d = 10;
%       f = @(X) sum(X(:,1:d/2),2)+sum(X(:,d/2+1:d),2).^4; ranks 2 and 5
% 
%       n_uni = 10;
%       n = n_uni*ones(1,d);
% 
%       p = 3000;  % smapling size
%       p2 = 50;   % validation size
%       pc = 1000; % test size
% 
%       W.P = randi(n_uni,p,d);
%       W.M_P = f(W.P);
% 
%       W.P2 = randi(n_uni,p2,d);
%       W.M_P2 = f(W.P2);
% 
%       W.C = randi(n_uni,pc,d);
%       W.M_C = f(W.C);
% 
%       para = struct;
%       para.rand_seed = 1;
%       para.print_info = 10; % plot singular values every 10 iterations
%       para.show_sing_spec = 5; % print every 5 iterations
%       para.movie = 1;
% 
%       SALSA(d,n,W,para);
%
%
%   For theoretical details, we refer to the corresponding paper [].
%
%   Author and License:
%
%   Written by Sebastian Kraemer, IGPM RWTH Aachen University (2019)
%   In case of problems or questions, feel free to send an email to
%   kraemer@igpm.rwth-aachen.de
%
%   See the license file for copyright issues.
%
%   See also: TEST_SALSA_JOBS, BENCHMARK

%% Default Parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function assign_default(field,default)
        if ~isfield(para,field)
            para.(field) = default;
        end
    end

% Rank adaption:
assign_default('r_lim',20);   
assign_default('r_uni_start',1);
assign_default('omega_0',0.5);
assign_default('f_omega_start',1.02);
assign_default('f_omega_min',1.0005);
assign_default('f_omega_max',1.1);
assign_default('num_minor_sing_vals',2);
assign_default('sigma_min_fac',1/10);
assign_default('rate_of_progress',0.005);
% Stopping criteria:
assign_default('iter_min',10);
assign_default('maxit',10000);
assign_default('res_min',1e-10);
assign_default('res_p2_max',5);
assign_default('omega_sigma_min_fac',1/50);
% Coarse CG:
assign_default('cg',1);
assign_default('cg_break_fac',0.01);
% Documentation (verbose):
assign_default('verbose',true);
per = 50;
assign_default('control',per);
assign_default('print_info',per);
assign_default('show_sing_spec',per);
assign_default('movie',false);


%% START
tic;

%% Initialization and pre-processing %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
G = []; % representation
r = []; % rank (note that the index is shifted by 1: r(1) := 1 =: r(d+1))
r_stab = ones(1,d+1);

%% Transfer problem
P = W.P;            % sampling set
P2 = W.P2;          % known control set
if isfield(W,'C') && isfield(W,'M_C') && ~isequal(W.C,[]) && ~isequal(W.M_C,[])
    C = W.C;        % "unknown" verification set
    M_C = W.M_C;    % target tensor on unknown control set C
else
    C = 1;
    M_C = 1;
    para.control = 0;
end
M_P = W.M_P;        % target tensor on sampling set P
M_P2 = W.M_P2;      % target tensor on known control set P2

[p,~] = size(P);
[pc,~] = size(C);
[p2,~] = size(P2);

[A_C_ch,A_C] = deal([]);
r_lim = para.r_lim;    % maximal technical rank
I_ = prod(n);

%% Symbols for ranks over 9...!
% just for display
ALP = cell(1,9+2*24);
for ii = 1:9 % 1 - 9
    ALP{ii} = char(48+ii);
end
for ii = 1:24 % > 9
    ALP{9+ii} = char(64+ii);
    ALP{9+24+ii} = char(96+ii);
end

%% Shortlists
% When updating mode s, the residual S and accordant arrays
% are sorted such that the corresponding entries of P(:,s) are
% non decreasing ( P(D(s,:),s) is sorted ). P itself is not changed.
% In Matlab, the gained speedup is questionable,
% but it is kept in analogy to other code.
% For 2-dimensional cores, additional permutations have to be saved,
% which are only used if para.dmrg == 1.
[D,D_inv,Dpart] = deal([]);
[D2,D_inv2,Dpart2] = deal([]);
create_shortlists();

%% Left and right evaluation of G on P and P2
% Qp(1:r(s),i,s) = G_1(p^D(s,i)_1)...G_{s-1}(p^D(s,i)_{s-1})
% where p^j is the j-th point in P.
[Qp,Pp] = deal(ones(r_lim,p,d));
[Qp2,Pp2] = deal(ones(r_lim,p2,d));

%% Residual vectors
S = zeros(p,1);     % on P
S2 = zeros(p2,1);   % on P2
Sc = zeros(pc,1);   % on C

%% Singular spectrum and filtering
sing_spec = zeros(r_lim,d+1);
sing_spec(1,1) = 1; sing_spec(1,d+1) = 1;
sing_spec_prior = sing_spec;
[omega_memory,res_p_memory,res_p2_memory] = deal(cell(2*d,1));
f_omega_part = zeros(2*d,1);
f_omega_part(:) = para.f_omega_start;
max_fact = -Inf;

Sigma = cell(d+1,1); % singular values
Sigma{1} = 1; Sigma{d+1} = 1;
[U_,V_] = deal(cell(d,1));

init_mean = mean(abs(M_P));
omega = para.omega_0*init_mean*sqrt(I_); 

omega_plain = omega / (init_mean * sqrt(I_));
omega_start = omega;
sigma_min = [];

[decide_incr,is_new_iter] = deal(zeros(1,d+1));
micro_step_nr = 0;
micro_step_max = 0;

f_omega = para.f_omega_start;
max_f_omega = Inf;
min_f_omega = -Inf;

prod_f_omega = 1;
prod_f_omega_count = 0;

[Yl,Yk,Ylk] = deal(zeros(1,d));

%% Others
[normM_P,res_p] = deal(norm(M_P)); % res_p is norm of residual on P
[normM_P2,res_p2] = deal(norm(M_P2)); % res_p2 is norm of residual on P2
[normM_C,res_c] = deal(norm(M_C)); % res_c is norm of residual on C

% previous and minimal residuals:
[res_p_old,res_g_old,res_p2_ch,res_p_ch,res_c_ch] = deal(2*res_p);
% last 5 residual reduction fectors regarding res_p/p2:
[quohist,quohist_p2] = deal(zeros(5,1));

% G: representation
% G_ch: best chosen representation according to res_p2
% h_crt: G is in general orthogonal with respect to h_crt
% dir: 1: sweep to the right, -1: sweep to the left (in main)
[G_ch,h_crt,dir,h_ch,r_ch] = deal([]);
iter = 0;
[r_old,r_min] = deal(zeros(1,d+1));

side = 0; % 1: sweep to the right, -1: sweep to the left (in center_to)
maxmaxsig = -Inf; minminsig = Inf; % for plotting

%% MOVIE
if para.movie
    i_movie = 0;
    MOVIE = struct('cdata',[],'colormap',[]);
end

%% CG
INT_cg = 0;
INT_cg_count = 0;


%% Execute MAIN -----------------------------------------------------------
main()

% movie:
if para.movie && para.show_sing_spec
    fprintf('\nMovie recorded and saved under movie.mat\n');
    save('movie','MOVIE');
    if exist('movie2gif') %#ok<EXIST>
        movie2gif(MOVIE, 'movie.gif', 'LoopCount', 5, 'DelayTime', 0.05*para.show_sing_spec);
        fprintf('Converted and saved movie as .gif file. \n');
    else
        fprintf('Download movie2gif from mathworks.com to convert movie into .gif file. \n');
    end
end

%% Stop timer
time = toc;

%% Save output
info = struct(...
    'res_c_ch',res_c_ch,...
    'res_p2_ch',res_p2_ch,...
    'res_p_ch',res_p_ch,...
    'res_c',res_c,...
    'res_p2',res_p2,...
    'res_p',res_p,...
    'time',time,...
    'iter',iter);
info.G = G;
info.G_ch = G_ch;
info.r = r;
info.r_ch = r_ch;
info.A_C = A_C;
info.A_C_ch = A_C_ch;

%% END --------------------------------------------------------------------
% -------------------------------------------------------------------------

%% FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% MAIN and init: ------------------------------------------------------------------

    function main()
        
        %% Set filter factors
        for s2 = 1:d
            k = sum(n(1:s2-1));
            l = sum(n(s2+1:d));
            ns = n(s2);
            
            Yl(s2) = l/(ns+k+l);
            Yk(s2) = k/(ns+k+l);
            Ylk(s2) = k*l/(ns+k+l)^2;
        end
        
        h_ = 2; h = [1,floor(d/2)+1; % use half-alternating sweeps
            d,floor(d/2)+1];
        % h_ = 2; h = [2,d; d-1,1]; % full alternating (cylon)
        % h_ = 1; h = [1,d]; % onside full alternating (typewriter)
        
        init(); % initialize respresentation G equiv 1
        do_break = false;
        
        while ~do_break && iter < para.maxit
            iter = iter + 1; 
            is_new_iter(:) = 1;
            micro_step_nr = 0;
            
            for i_h = 1:h_ % proceed one sweep
                dir = sign(h(i_h,2)-h(i_h,1));
                for s = h(i_h,1):dir:h(i_h,2)                                                                
                    %% count micro step numbers
                    micro_step_nr = micro_step_nr + 1;
                    micro_step_max = max(micro_step_max,micro_step_nr);
                    
                    %% orthogonalize, change mode and check for rank increases:
                    center_to(s,'check');
                    check_if_centered_to(s);    

                    %% optimize single core:
                    opt_block(s);
  
                    %% set singular values to a minimum   
                    update_sing_vals(s);
                    
                    %% update residuals
                    update_S(h_crt);
                    update_S2(h_crt);
                    
                    %% adapt speed of decrease of omega
                    update_r_stab()
                    if iter > 1
                        adapt_f_omega();
                    end
                    
                    %% remember values to calculate progress
                    omega_memory{micro_step_nr} = omega;
                    res_p_memory{micro_step_nr} = res_p;
                    res_p2_memory{micro_step_nr} = res_p2;
                    
                    %% adapt (decrease) filter parameter:
                    adapt_sigma_min();
                    adapt_omega();
                end
            end
            
            keep_book(); % save residual reduction factors
            if para.verbose
                document() % for additional information
            end
            
            do_break = check_impr(); % check for final break
        end
        
        % reduce G{s} and G_ch{s} to their actual sizes
        for s = 1:d
            G{s} = G{s}(1:r(s),1:r(s+1),:);
            G_ch{s} = G_ch{s}(1:r_ch(s),1:r_ch(s+1),:);
        end
        
        % check residuals of last representation
        update_S(h_crt);
        if ~isequal(C,1)
            A_C = control();
        end
        update_S2(h_crt);
        
        % check residuals of chosen representation
        res_p_ch = control_res(G_ch,r_ch,P,M_P);
        res_p2_ch = control_res(G_ch,r_ch,P2,M_P2);
        if ~isequal(C,1)
            [res_c_ch,A_C_ch] = control_res(G_ch,r_ch,C,M_C);
        end
        
        % print and save
        print_info();
        final_result();
    end


%% Optimization (single, cg and full) -------------------------------------

    function opt_block(s) % single (standard) mode optimization
        Dps = Dpart{s}(:);
        
        V_{s} = 1; U_{s} = 1; % update sing. vals of missing side
        if side == 1
            if s~=d
                [~,Sigma{s+1},V_{s}] = left_SVD_G(s);
                sing_spec(1:r(s+1),s+1) = diag(Sigma{s+1});
            end
        else
            if s~=1
                [U_{s},Sigma{s},~] = right_SVD_G(s);
                sing_spec(1:r(s),s) = diag(Sigma{s});
            end
        end
        w = omega;
        
        % limit singular values from below with sigma_min
        Sigma_wlim = Sigma;
        f_ = @(sigma,sigma_min) max(sigma,sigma_min);
        
        for i = 1:r(s+1)
            Sigma_wlim{s+1}(i,i) = f_(Sigma{s+1}(i,i),sigma_min);
        end
        for i = 1:r(s)
            Sigma_wlim{s}(i,i) = f_(Sigma{s}(i,i),sigma_min);
        end
        
        %% solve least squares exactly
        if ~para.cg || iter == 1 || r(s)*r(s+1) == 1
            for is = 1:n(s)
                % range of points with p_{s} = is
                v = Dps(is):Dps(is+1)-1;
                % filtered or (for first iteration) not filtered
                if iter > 1
                    % fill in F (F G_s = tau(G)_P)
                    F(:,1:(Dps(is+1)-Dps(is))) = reshape(multiprod(Qp(1:r(s),v,s),Pp(1:r(s+1),v,s),[1,0],[0,1]),...
                        [r(s)*r(s+1),Dps(is+1)-Dps(is)]);
                    
                    if s ~= 1
                        Wl = sqrt(Yl(s)*w^2/prod(n(s+1:d)))*kron(diag(diag(Sigma_wlim{s+1}).^(-1))*V_{s}',Qp(1:r(s),v,s)');
                        Wl_zero = zeros((Dps(is+1)-Dps(is))*r(s+1),1);
                    else
                        Wl = sqrt((Dps(is+1)-Dps(is))*Yl(s)*w^2/prod(n(s+1:d)))*diag(diag(Sigma_wlim{s+1}).^(-1))*V_{s}';
                        Wl_zero = zeros(r(s+1),1);
                    end
                    if s ~= d
                        Wk = sqrt(Yk(s)*w^2/prod(n(1:s-1)))*kron(Pp(1:r(s+1),v,s)',diag(diag(Sigma_wlim{s}).^(-1))*U_{s}');
                        Wk_zero = zeros((Dps(is+1)-Dps(is))*r(s),1);
                    else
                        Wk = sqrt((Dps(is+1)-Dps(is))*Yk(s)*w^2/prod(n(1:s-1)))*diag(diag(Sigma_wlim{s}).^(-1))*U_{s}';
                        Wk_zero = zeros(r(s),1);
                    end
                    Wlk = sqrt(Ylk(s)*w^4/I_*n(s)*(Dps(is+1)-Dps(is)))*kron(diag(diag(Sigma_wlim{s+1}).^(-1))*V_{s}',diag(diag(Sigma_wlim{s}).^(-1))*U_{s}');
                    
                    g = [F(:,1:(Dps(is+1)-Dps(is)))';Wl;Wk;Wlk]\[M_P(D(s,v)); Wl_zero; Wk_zero; zeros(r(s)*r(s+1),1)];
                else
                    % fill in F (F G_s = tau(G)_P)
                    F(:,1:(Dps(is+1)-Dps(is))) = reshape(multiprod(Qp(1:r(s),v,s),Pp(1:r(s+1),v,s),[1,0],[0,1]),...
                        [r(s)*r(s+1),Dps(is+1)-Dps(is)]);
                    
                    g = F(:,1:(Dps(is+1)-Dps(is)))' \ M_P(D(s,v));
                end
                
                % reshape into matrix
                grad = reshape(g,[r(s),r(s+1)]);
                
                % embedd into R^P
                z_grad = sum((grad'*Qp(1:r(s),v,s)).*Pp(1:r(s+1),v,s),1)';
                
                G{s}(1:r(s),1:r(s+1),is) = grad;
                S(v) = M_P(D(s,v)) - z_grad;
            end
            %% solve ls-problem approximatly via cg (as in "Dahmen/Reusken")
            %
            % minimize_x  1/2 x'(A'A + B'B)x - (A'S(v) - B'x0)
            % or equivalently  \|[A;B](x0 + x) - [M(v),0]\| via cg
            %
            % A is the map from core s to the tensor on sampling points in P(v)
            % B is the regularization matrix
            % S(v) is the residual tensor on sampling points in P(v)
            % M(v) is the target tensor on sampling points in P(v)
            %
            % Preconditioning with divisor <-> R'R ~ A'A + B'B based on orthogonality
            % assumptions on Qp and Pp
            %
        else
            
            % preconditioner (is a diagonal matrix):
            VTSSV = diag(diag(Sigma_wlim{s+1}).^(-2));
            USSUT = diag(diag(Sigma_wlim{s}).^(-2));
            
            divisor = ( 1/I_*n(s)*ones(r(s),r(s+1)) ...
                + Yl(s)*w^2/prod(n(s+1:d)) * (1/prod(n(1:s-1))*ones(r(s),1)) * (diag(Sigma_wlim{s+1}).^(-2))' ...
                + Yk(s)*w^2/prod(n(1:s-1)) * (diag(Sigma_wlim{s}).^(-2)) * (1/prod(n(s+1:d))*ones(1,r(s+1))) ...
                + Ylk(s)*w^4/I_*n(s) * ((diag(Sigma_wlim{s}).^(-2))) * (diag(Sigma_wlim{s+1}).^(-2))');
            
            for is = 1:n(s)
                
                v = Dps(is):Dps(is+1)-1; % range of points with p_{s} = is
                
                % multiply V and U into Qp and Pp (Pp -> V Pp, Qp -> Qp U, G -> U' * G * V')
                x0 = U_{s}' * G{s}(1:r(s),1:r(s+1),is) * V_{s};
                
                % for matrix BTB
                QpTQp = U_{s}' *   Qp(1:r(s),v,s)*Qp(1:r(s),v,s)'   * U_{s};
                PpTPp = V_{s}'  * Pp(1:r(s+1),v,s)*Pp(1:r(s+1),v,s)' * V_{s};
                
                BTBx0 = Yl(s)*w^2/prod(n(s+1:d))* QpTQp * x0 * VTSSV  + ...
                    Yk(s)*w^2/prod(n(1:s-1))* USSUT * x0 * PpTPp + ...
                    Ylk(s)*w^4/I_*n(s)*(Dps(is+1)-Dps(is))* USSUT * x0 * VTSSV;
                
                % gradient + preconditioning
                grad = U_{s}' * bsxfun(@times,Qp(1:r(s),v,s),S(v)') * Pp(1:r(s+1),v,s)' * V_{s} - BTBx0;
                deltag = grad./((Dps(is+1)-Dps(is))*divisor);
                
                % A * (x = deltag)
                z_grad = sum(( (U_{s} * deltag * V_{s}')' *Qp(1:r(s),v,s)).*Pp(1:r(s+1),v,s),1)';
                
                % BTB * (x = deltag)
                BTBdeltag = Yl(s)*w^2/prod(n(s+1:d))* QpTQp * deltag * VTSSV ...
                    + Yk(s)*w^2/prod(n(1:s-1))* USSUT *deltag* PpTPp ...
                    + Ylk(s)*w^4/I_*n(s)*(Dps(is+1)-Dps(is))* USSUT *deltag* VTSSV;
                
                % calculate alpha
                rkTrk = deltag(:)'*grad(:); % = z_grad'*S(v) - deltag(:)'*BTBx0(:)   (new)
                sqrtrkTrk0 = sqrt(rkTrk);
                vTv = z_grad'*z_grad + deltag(:)'*BTBdeltag(:);
                
                alpha = rkTrk/vTv;
                
                % adapt x0 and residual
                S(v) = S(v) - alpha*z_grad;
                x0 = x0 + alpha*deltag;
                
                %% for next step
                int_cg = 1;
                tol_reached = 0;
                while ~tol_reached && int_cg <= r(s)*r(s+1)
                    
                    % include change on x0
                    BTBx0 = BTBx0 + alpha*BTBdeltag;
                    
                    % save previous values
                    rkTrk_last = rkTrk;
                    deltag0 = deltag;
                    
                    grad = U_{s}' * bsxfun(@times,Qp(1:r(s),v,s),S(v)') * Pp(1:r(s+1),v,s)' * V_{s} - BTBx0;
                    deltag = grad./((Dps(is+1)-Dps(is))*divisor);
                    
                    rkTrk = deltag(:)'*grad(:);
                    
                    % breaking criterion
                    if sqrt(rkTrk) > para.cg_break_fac*sqrtrkTrk0
                        
                        % calculate correction with factor beta
                        beta = rkTrk / rkTrk_last;
                        deltag = deltag + beta*deltag0;
                        
                        % A * (x = deltag)
                        z_grad = sum(( (U_{s} * deltag * V_{s}')' *Qp(1:r(s),v,s)).*Pp(1:r(s+1),v,s),1)';
                        
                        % BTB * (x = deltag)
                        BTBdeltag = Yl(s)*w^2/prod(n(s+1:d))* QpTQp * deltag * VTSSV ...
                            + Yk(s)*w^2/prod(n(1:s-1))* USSUT *deltag* PpTPp ...
                            + Ylk(s)*w^4/I_*n(s)*(Dps(is+1)-Dps(is))* USSUT *deltag* VTSSV;
                        
                        % calculate alpha
                        vTv = z_grad'*z_grad + deltag(:)'*BTBdeltag(:);
                        alpha = rkTrk/vTv;
                        
                        % adapt x0 and residual
                        S(v) = S(v) - alpha*z_grad;
                        x0 = x0 + alpha*deltag;
                        
                        % count cg iterations
                        int_cg = int_cg + 1;
                    else
                        tol_reached = 1;
                    end
                    
                end
                
                INT_cg = INT_cg + int_cg;
                INT_cg_count = INT_cg_count + 1;
                
                G{s}(1:r(s),1:r(s+1),is) = U_{s} * x0 * V_{s}';
            end
        end
    end


%% Init and Termination ---------------------------------------------------------

    function init()
        if isequal(para.r_uni_start,0)
            r = ones(1,d+1);
        else
            r = [1,ones(1,d-1)*para.r_uni_start,1]; 
        end
        G = cell(1,d);
        
        for s = 1:d
            G{s} = zeros(r_lim,r_lim,n(s));
            
            for i = 1:para.r_uni_start
                
                if s == 1 && i == 1
                    fac = init_mean; 
                elseif s == 1
                    fac = 1e-3*init_mean; %sigma_min/sqrt(I_); % = 1e-2*para.omega_0*normal_P/sqrt(p)
                else
                    fac = 1;
                end
                
                if i == 1
                    G{s}(1,1,:) = fac;
                else
                    G{s}(min(r(s),i),min(r(s+1),i),:) = fac*rand(1,n(s));
                end
            end
        end
 
        h_crt = 1;
        center_to(d,'blanc');
        center_to(1,'blanc');
        update_S(h_crt);
        update_S2(h_crt);
        
        normA = norm(sing_spec(:,2:d),'fro')/sqrt(d-1);
        omega = para.omega_0*normA;
        omega_plain = omega/normA; 
        
        adapt_sigma_min();
    end

    function create_shortlists() % save permutations
        %% P
        D = zeros(d,p); D_inv = zeros(d,p);
        Dpart = cell(1,d);
        
        for s = 1:d
            Dpart{s} = [zeros(1,n(s)),p+1];
            [a,D(s,:)] = sort(P(:,s));
            D_inv(s,D(s,:)) = 1:p;
            for is = 1:n(s)
                temp = find(a>=is,1,'first');
                if isempty(temp)
                    Dpart{s}(is) = p+1;
                else
                    Dpart{s}(is) = temp;
                end
            end
        end
        
        %% P2
        D2 = zeros(d,p2); D_inv2 = zeros(d,p2);
        Dpart2 = cell(1,d);
        
        for s = 1:d
            Dpart2{s} = [zeros(1,n(s)),p2+1];
            [a,D2(s,:)] = sort(P2(:,s));
            D_inv2(s,D2(s,:)) = 1:p2;
            for is = 1:n(s)
                temp = find(a>=is,1,'first');
                if isempty(temp)
                    Dpart2{s}(is) = p2+1;
                else
                    Dpart2{s}(is) = temp;
                end
            end
        end
    end

    function do_break = check_impr()
        do_break = true;
         
        % divergence
        if iter > para.iter_min && res_p2 > res_p2_ch*para.res_p2_max && min(r(2:d)) > 1
            fprintf('Divergence on control set. \n');
            return;
        end
        
        % convergence
        if omega < 1e-10*omega_start || res_p/normM_P < para.res_min
            fprintf('Convergence of omega or res_p to 0. \n');
            return;
        end
        
        % stagnation
        if iter > para.iter_min && omega < para.omega_sigma_min_fac*para.sigma_min_fac*sqrt(I_/p2)*res_p2 && ...
                abs(f_omega-para.f_omega_max)/para.f_omega_max < 1e-3  
            fprintf('Stagnation of optimization. \n');
            return;
        end
        
        do_break = false;        
    end

    function val = get_progr_p()
        val = 1 - sum(min(1,quohist))/5;
    end

    function val = get_progr_p2()
        val = 1 - sum(min(1,quohist_p2))/5;
    end


%% Mode changes (orthogonalization) ---------------------------------------

    function check_if_centered_to(h) % control routine
%         (can stay commented to safe time without changing outcome)
%         a = zeros(1,d);
%         for s = 1:h-1
%             H = left_fold(s)'*left_fold(s);
%             a(s) = norm(H-eye(size(H,1)),'fro');
%         end
%         for s = h+1:d
%             H = right_fold(s)*right_fold(s)';
%             a(s) = norm(H-eye(size(H,1)),'fro');
%         end
%         [maxa,ind] = max(a);
%         if maxa > 1e-12
%             fprintf('NOT %d orthogonal, badness %2.2e in node %d \n',h,maxa,ind);
%         end
    end

    function center_to(h_end,task)
        % orthogonalize with respect to h_end if
        % orthogonal with respect to h_crt
        if (h_crt < h_end)
            move_left(h_crt,h_end,task);
        elseif (h_end < h_crt)
            move_right(h_crt,h_end,task);
        end
    end

    function move_left(h_start,h_end,task) % left-orthogonalize left blocks
        
        for s = h_start:h_end-1
            side = 1;
            rank_was_changed = false;
            [U,Sigma{s+1},V] = left_SVD_G(s);
            sigma = diag(Sigma{s+1});
            
            left_ASSIGN_G(U,s);
            left_MUL_G(V',s+1);
            
            if rank_is_increased(s+1) && strcmp(task,'check') && iter > 2
                sigma = add_minor_sing_val(sigma,s+1);
                % G{s}, G{s+1} have been modified
                % r(s+1) has been increased
                
                rank_was_changed = true;
                decide_incr(s+1) = 0;
            elseif rank_is_decreased(s+1) && strcmp(task,'check')
                sigma = reduce_rank(sigma,s+1);
                % G{s}, G{s+1} have been modified
                % r(s+1) has been decreased
                
                rank_was_changed = true;
            end
                           
            Sigma{s+1} = diag(sigma);
            
            left_MUL_G(Sigma{s+1},s+1);
            
            % carry on branch computations
            GET_Qp_(s+1);
            GET_Qp2_(s+1);
            
            if ~rank_was_changed 
                S(D_inv(s+1,:)) = S(D_inv(s,:));
            else
                update_S(s+1);
            end
            h_crt = s+1;
        end
    end

    function move_right(h_start,h_end,task) % right-orthogonalize right blocks
        for s = h_start:-1:h_end+1
            side = -1;
            rank_was_changed = false;
            [U,Sigma{s},V] = right_SVD_G(s);
            sigma = diag(Sigma{s});
            
            right_ASSIGN_G(s,V');
            right_MUL_G(s-1,U);
            
            if rank_is_increased(s) && strcmp(task,'check') && iter > 2
                sigma = add_minor_sing_val(sigma,s);
                % G{s-1}, G{s} have been modified
                % r(s) has been increased
                
                rank_was_changed = true;
                decide_incr(s) = 0;
            elseif rank_is_decreased(s) && strcmp(task,'check')
                sigma = reduce_rank(sigma,s);
                % G{s-1}, G{s} have been modified
                % r(s) has been decreased
                
                rank_was_changed = true;
            end
            
            Sigma{s} = diag(sigma);
            right_MUL_G(s-1,Sigma{s});
            
            % carry on branch computations
            GET_Pp_(s-1);
            GET_Pg2_(s-1);
            
            if ~rank_was_changed
                S(D_inv(s-1,:)) = S(D_inv(s,:));
            else
                update_S(s-1);
            end
            h_crt = s-1;
        end
    end
        
    function GET_Qp_(s) % add next factors
        for is = 1:n(s-1)
            Qp(1:r(s),Dpart{s-1}(is):Dpart{s-1}(is+1)-1,s) = G{s-1}(1:r(s-1),1:r(s),is)' * Qp(1:r(s-1),Dpart{s-1}(is):Dpart{s-1}(is+1)-1,s-1);
        end
        Qp(1:r(s),D_inv(s,:),s) = Qp(1:r(s),D_inv(s-1,:),s); % permute to fit next optimisation
    end

    function GET_Qp2_(s) % add next factors
        for is = 1:n(s-1)
            Qp2(1:r(s),Dpart2{s-1}(is):Dpart2{s-1}(is+1)-1,s) = G{s-1}(1:r(s-1),1:r(s),is)' * Qp2(1:r(s-1),Dpart2{s-1}(is):Dpart2{s-1}(is+1)-1,s-1);
        end
        Qp2(1:r(s),D_inv2(s,:),s) = Qp2(1:r(s),D_inv2(s-1,:),s); % permute to fit next optimisation
    end

    function GET_Pp_(s) % add next factors
        for is = 1:n(s+1)
            Pp(1:r(s+1),Dpart{s+1}(is):Dpart{s+1}(is+1)-1,s) = G{s+1}(1:r(s+1),1:r(s+2),is) * Pp(1:r(s+2),Dpart{s+1}(is):Dpart{s+1}(is+1)-1,s+1);
        end
        Pp(1:r(s+1),D_inv(s,:),s) = Pp(1:r(s+1),D_inv(s+1,:),s);
    end

    function GET_Pg2_(s) % add next factors
        for is = 1:n(s+1)
            Pp2(1:r(s+1),Dpart2{s+1}(is):Dpart2{s+1}(is+1)-1,s) = G{s+1}(1:r(s+1),1:r(s+2),is) * Pp2(1:r(s+2),Dpart2{s+1}(is):Dpart2{s+1}(is+1)-1,s+1);
        end
        Pp2(1:r(s+1),D_inv2(s,:),s) = Pp2(1:r(s+1),D_inv2(s+1,:),s);
    end


%% Sampling related tensor evaluations

    function A = control() % calculate res_c
        Y = G{1}(1:r(1),1:r(2),C(:,1));
        for s = 2:d
            Y = multiprod(Y,G{s}(1:r(s),1:r(s+1),C(:,s)));
        end
        A = Y(:);
        Sc = M_C - A;
        res_c = norm(Sc);
    end

    function [res_control,A] = control_res(G,r,P,M_P)
        % calculate residual without using Qp and Pp
        Y = G{1}(1:r(1),1:r(2),P(:,1));
        for s = 2:d
            Y = multiprod(Y,G{s}(1:r(s),1:r(s+1),P(:,s)));
        end
        A = Y(:);
        SP = M_P - A;
        res_control = norm(SP);
    end

    function update_S(h) % calculate res_p
        for is = 1:n(h)
            v = Dpart{h}(is):Dpart{h}(is+1)-1;
            
            S(v) = M_P(D(h,v)) - ...
                sum((G{h}(1:r(h),1:r(h+1),is)'*Qp(1:r(h),v,h)).*Pp(1:r(h+1),v,h),1)';
        end
        res_p = norm(S);
    end

    function update_S2(h) % calculate res_p2
        for is = 1:n(h)
            if Dpart2{h}(is+1)~=Dpart2{h}(is)
                v = Dpart2{h}(is):Dpart2{h}(is+1)-1;
                
                
                S2(v) = M_P2(D2(h,v)) - ...
                    sum((G{h}(1:r(h),1:r(h+1),is)'*Qp2(1:r(h),v,h)).*Pp2(1:r(h+1),v,h),1)';
            end
        end
        res_p2 = norm(S2);
        if res_p2_ch > res_p2
            res_p2_ch = res_p2;
            res_p_ch = res_p;
            res_c_ch = res_c;
            r_min = r;
            G_ch = G;
            h_ch = h_crt;
            r_ch = r;
        end
    end


%% Tensor operations ------------------------------------------------------

    function L = left_fold(s) % get \mathfrac{L}(G_s) (unused in this version)
        L = reshape(permute(...
            G{s}(1:r(s),1:r(s+1),:),[1,3,2]),...
            [r(s)*n(s),r(s+1)]);
    end

    function R = right_fold(s) % get \mathfrac{R}(G_s) (unused in this version)
        R = reshape(G{s}(1:r(s),1:r(s+1),:),[r(s),r(s+1)*n(s)]);
    end

    function [U,Sigma,V] = left_SVD_G(s) % get SVD of \mathfrac{L}(G_s)
        [U,Sigma,V] = svd(reshape(permute(G{s}(1:r(s),1:r(s+1),:),[1,3,2]),[r(s)*n(s),r(s+1)]),'econ');
    end

    function [U,Sigma,V] = right_SVD_G(s) % get SVD of \mathfrac{R}(G_s)
        [U,Sigma,V] = svd(reshape(G{s}(1:r(s),1:r(s+1),:),[r(s),r(s+1)*n(s)]),'econ');
    end

    function left_MUL_G(R,s) % R*G_s
        G{s}(1:r(s),1:r(s+1),:) =  reshape(R * reshape(...
            G{s}(1:r(s),1:r(s+1),:),[r(s),r(s+1)*n(s)]),...
            [r(s),r(s+1),n(s)]);
    end

    function right_MUL_G(s,L) % G_s*L
        G{s}(1:r(s),1:r(s+1),:) = permute(reshape(reshape(permute(...
            G{s}(1:r(s),1:r(s+1),:),[1,3,2]),...
            [r(s)*n(s),r(s+1)])*L,...
            [r(s),n(s),r(s+1)]),...
            [1,3,2]);
    end

    function left_ASSIGN_G(U,s) % \mathfrac{L}(G_s) <- \mathfrac{L}(U)
        G{s}(1:r(s),1:r(s+1),:) = permute(reshape(U,[r(s),n(s),r(s+1)]),[1,3,2]);
    end

    function right_ASSIGN_G(s,Vt) % \mathfrac{R}(G_s) <- \mathfrac{R}(Vt)
        G{s}(1:r(s),1:r(s+1),:) = reshape(Vt,[r(s),r(s+1),n(s)]);
    end
 

%% Rank increases and cuts, singular value and omega parameter related -----------------------------

    function val = res_border_estimate() 
        val_p = res_p/sqrt(p)*sqrt(I_);
        % val_p2 = res_p2/sqrt(p2)*sqrt(I_);
        
        val = val_p; 
    end

    function update_sing_vals(s) % limits neighbouring singular values
        
        if s~= d
            [~,Sigma{s+1},~] = left_SVD_G(s);           
            sigma_ = diag(Sigma{s+1});
            minor_sv = is_minor_sv(sigma_);
            
            if sum(minor_sv) < para.num_minor_sing_vals 
                decide_incr(s+1) = decide_incr(s+1) + is_new_iter(s+1);
                is_new_iter(s+1) = 0;
            else
                decide_incr(s+1) = 0;
            end
            
            % save sing vals to singspec
            sing_spec(1:r(s+1),s+1) = diag(Sigma{s+1});
        end
        
        if s~=1
            [~,Sigma{s},~] = right_SVD_G(s);
            
            sigma_ = diag(Sigma{s});
            minor_sv = is_minor_sv(sigma_);
            
            if sum(minor_sv) < para.num_minor_sing_vals 
                decide_incr(s) = decide_incr(s) + is_new_iter(s);
                is_new_iter(s) = 0;
            else
                decide_incr(s) = 0;
            end
            
            % save sing vals to singspec 
            sing_spec(1:r(s),s) = diag(Sigma{s});
        end
    end

    function update_r_stab()
        for s = 2:d
            novsv_s = sum(is_minor_sv(sing_spec(1:r(s),s)));
            r_stab(s) = r(s) - novsv_s;
        end
    end

    function adapt_f_omega()
        % adapt f_omega_part such that progress tends to become 1e-2
        
        % compare progress rates
        rop = max(min(1,res_p/res_p_memory{micro_step_nr}),min(1,res_p2/res_p2_memory{micro_step_nr}));
        fac = log(rop)/log(1-para.rate_of_progress);
        
        % avoid devision by zero
        fac = max(fac,1e-2);
        
        old_f_omega_part = (omega_memory{micro_step_nr}/omega)^(1/micro_step_max);
        f_omega_part_new = old_f_omega_part^(1/fac);
        
        % limit from below and above
        f_omega_part_new = max(para.f_omega_min^(1/micro_step_max),min(para.f_omega_max^(1/micro_step_max),f_omega_part_new));
        
        f_omega_part(micro_step_nr) = f_omega_part_new;
        f_omega_new = prod(f_omega_part(1:micro_step_max));
        
        % record extremal values for print out
        max_f_omega = max(max_f_omega,f_omega_new);
        min_f_omega = min(min_f_omega,f_omega_new);
        
        f_omega = f_omega_new;
    end

    function adapt_sigma_min()
        sigma_min = para.sigma_min_fac*res_border_estimate();
    end

    function adapt_omega()
        if iter == 1
            omega_start = omega;
        end
        
        % decrease norm independent parameter (only ever decreases)
        omega_plain = omega_plain/(f_omega^(1/micro_step_max));
        
        % (remember decrease for print output)
        prod_f_omega = prod_f_omega * f_omega;
        prod_f_omega_count = prod_f_omega_count + 1;
        
        % use maximal norm (only ever increases)
        normA = norm(sing_spec(:,2:d),'fro')/sqrt(d-1);
        max_fact = max(max_fact,normA);
        
        % adapt real parameter to norm of iterate
        omega = omega_plain * max_fact;
        
        % ensure omega is not too large and adapt norm independent 
        % parameter to change of omega
        omega = min([omega,res_border_estimate(),para.omega_0*normA]);      
        omega_plain = min(omega_plain,omega/max_fact);
    end

    function increase_rank = rank_is_increased(s) 
        increase_rank = false;

        % if ~(condition)
        %   return
        % end
        %
        % means: after every such condition is met => increase_rank = true
        
        if ~(r(s) < para.r_lim)
            return;
        end
        
        if ~(decide_incr(s) > 2)             
            return
        end
        
        % check if rank increase is possible according to mode sizes:          
        if ~(r(s-1)*n(s-1) >= r(s)+1 && r(s+1)*n(s) >= r(s)+1)
            return;
        end
       
        increase_rank = true;
    end

    function do_decrease = rank_is_decreased(s)
        do_decrease = false;
        if r(s) - r_stab(s) > para.num_minor_sing_vals && r(s) > para.r_uni_start
            % check if rank decrease is possible according to mode sizes:
            if ~(r(s-1)*n(s-1) >= r(s)-1 && r(s+1)*n(s) >= r(s)-1)
                return;
            end
 
            do_decrease = true;
        end     
    end

    function sigma_new = add_minor_sing_val(sigma,s)
        r_new = r; r_new(s) = r_new(s)+1;
        
        % add orthogonal columns
        L = left_fold(s-1); % column-orth
        vL = ones(r(s-1)*n(s-1),1);
        vL = vL - L*(L'*vL);
        vL = vL - L*(L'*vL); % twice to avoid round-off errors
        vL = vL/norm(vL);
        
        R = right_fold(s); % row-orth
        vR = ones(1,r(s+1)*n(s));
        vR = vR - (vR*R')*R;
        vR = vR - (vR*R')*R; % twice to avoid round-off errors
        vR = vR/norm(vR);
        
        r(s) = r_new(s);
        left_ASSIGN_G([L,vL],s-1);
        right_ASSIGN_G(s,[R;vR]);
        
        sigma_new = [sigma;1/100*sigma_min];          
    end

    function sigma_new = reduce_rank(sigma,s)
        r_new = r; r_new(s) = r_new(s)-1;
        
        L = left_fold(s-1); % column-orth
        L(:,end) = 0;
        
        R = right_fold(s); % row-orth
        R(end,:) = 0;
        
        left_ASSIGN_G(L,s-1);
        right_ASSIGN_G(s,R);
        
        sing_spec(r(s),s) = 0;
        r(s) = r_new(s);
        
        sigma_new = sigma(1:r(s));      
    end

    function logical_ind = is_minor_sv(sigma_unf)
        logical_ind = sigma_unf < 0.5*omega;
    end


%% Bookkeeping ------------------------------------------------------------

    function document()
        if mod(iter,para.control) == 0
            control();
        end
        if mod(iter,para.print_info) == 0
            print_info();
        end
        if mod(iter,para.show_sing_spec) == 0
            if iter == para.show_sing_spec
                figure(1)
            end
            plot_sing_spec()
        end
    end

    function keep_book()
        quohist(1:4) = quohist(2:5);
        quohist(5) = res_p/res_p_old; % < 1 if residual gets better
        res_p_old = res_p;
        quohist_p2(1:4) = quohist_p2(2:5);
        quohist_p2(5) = res_p2/res_g_old; % < 1 if residual gets better
        res_g_old = res_p2;
    end


%% Printing and plotting --------------------------------------------------

    function print_info()
        if max(r_old) < max(r) % max(r) increased since last print
            fprintf('[ %s max(r) = %d: ]  \n',char(148),max(r));
        end
        
        fprintf('r_stab: ');
        for s = 2:d     
            if r(s)~=r_old(s)
                fprintf('[%s]',ALP{max(1,r(s))});
            else
                fprintf(' %s ',ALP{max(1,r_stab(s))});
            end
        end

        r_old = r;        
        fprintf(' -- <int_cg>: %2d, max(r): %2d, iter: %4d, omega: %2.2e, f_omega: [%2.3f,%2.3f,%2.3f], progr_p: %2.2e, progr_p2: %2.2e, rresc: %2.2e(%2.2e), rresp2: %2.2e(%2.2e), rresp: %2.2e(%2.2e), t: %7.2f \n',...
            round(INT_cg/INT_cg_count),max(r),iter,omega,min_f_omega,prod_f_omega^(1/prod_f_omega_count),max_f_omega,get_progr_p(),get_progr_p2(),res_c/normM_C,max(abs(Sc./M_C)),res_p2/normM_C,max(abs(S2./M_P2)),res_p/normM_P,max(abs(S./M_P)),toc);
        INT_cg = 0;
        INT_cg_count = 0;
        prod_f_omega_count = 0;
        prod_f_omega = 1;
        max_f_omega = -Inf;
        min_f_omega = Inf;
    end

    function plot_sing_spec()
        
        blue = [0,83,159]/255;
        teal = [0,152,161]/255;
        bord = [161,16,53]/255;
        
        c = 2;
        ms = 4;
        lw = 2.5;
        
        resp_line = res_p/sqrt(p)*sqrt(I_);
        resp2_line = res_p2/sqrt(p2)*sqrt(I_);
        
        maxsig = max([resp_line,resp2_line,sigma_min,omega]);
        minsig = min([resp_line,resp2_line,sigma_min,omega]);
        for i = 2:d
            maxsig = max(max(diag(Sigma{i})),maxsig);
            minsig = min(min(diag(Sigma{i})),minsig);
        end
        minsig = 1/10*minsig;
        
        if iter > 2
            maxmaxsig = max(maxsig,maxmaxsig);
            minminsig = min(minsig,minminsig);
        else
            maxmaxsig = maxsig;
            minminsig = minsig;   
        end
        
        y_max = exp(4*ceil(1/4*log(maxmaxsig)));
        y_min = min(y_max/10,exp(4*floor(1/4*log(minminsig))));
        
        r_red = r;
        clf;
        
        for i = 2:d
            semilogy([i,i],[y_min,y_max],':','color',[0.7,0.7,0.7]); hold on;
            
            sings = diag(Sigma{i});
            minor_sv = is_minor_sv(sings);
            
            r_red(i) = max(1,sum(~minor_sv));
            
            if any(~minor_sv)
                semilogy(i,sings(~minor_sv)','o','color',blue,'LineWidth',lw,'MarkerFaceColor',blue,'MarkerSize',ms*c); hold on;
            end
            if any(minor_sv)
                semilogy(i,sings(minor_sv)','o','color',teal,'LineWidth',lw,'MarkerSize',ms*c); hold on;
                semilogy(i,sing_spec_prior(minor_sv,i)','o','color',blue,'MarkerFaceColor',blue,'LineWidth',lw,'MarkerSize',2/3*ms*c); hold on;
            end
        end
        
        text(-0.1,0.8*(resp_line),'res_p');
        semilogy([1,d+1],[resp_line,resp_line],'-','color',bord);
        text(-0.1,0.8*(resp2_line),'res_{p2}');
        semilogy([1,d+1],[resp2_line,resp2_line],'--','color',bord);
        text(-0.1,0.8*sigma_min,'\sigma_{min}');
        semilogy([1,d+1],[sigma_min,sigma_min],'-','color',bord);
        text(-0.1,0.8*omega,'\omega');
        semilogy([1,d+1],[omega,omega],'-','color',teal);
        axis([-0.5 d+1 y_min y_max]);

        set(gca,'XTick',2:d);
        set(gca,'XTickLabel',2:d);
        
        ylabel('singular values');
        xlabel('index');
        
        drawnow;
        
        if para.movie
            i_movie = i_movie + 1;
            MOVIE(i_movie) = getframe;
        end
        
    end

    function final_result()
        fprintf('\n   last result - r: ');
        for s = 1:d+1
            fprintf('%s',ALP{r(s)});
        end
        fprintf(' ,');
        
        fprintf('rresc: %2.2e, rresg: %2.2e, rresp: %2.2e (last) \n',...
            res_c/normM_C,res_p2/normM_P2,res_p/normM_P);
        
        fprintf('\n chosen result - r: ');
        for s = 1:d+1
            fprintf('%s',ALP{r_min(s)});
        end
        fprintf(' ,');
        fprintf('rresc: %2.2e, rresg: %2.2e, rresp: %2.2e (chosen) \n',...
            res_c_ch/normM_C,res_p2_ch/normM_P2,res_p_ch/normM_P);
        
        if para.show_sing_spec ~= 0
            plot_sing_spec()
        end
    end

end
