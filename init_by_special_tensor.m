function [M_P,M_P2,M_C,para] = init_by_special_tensor(para,P,P2,C)
% INIT_BY_SPECIAL_TENSOR generates the rank adaption test tensor as defined
% in the paper
%
%   Author:
%
%   Written by Sebastian Kraemer, IGPM RWTH Aachen University (2019)
%   In case of problems or questions, feel free to send an email to
%   kraemer@igpm.rwth-aachen.de
%
%   See the license file for copyright issues.

d = 6;
k = para.r_rnd;

para.d = d;
n = para.n;

G = cell(1,d);
r = [1,k,k,k,1,2*k,1];

main();

    function main()
        init();
        
        [U,~,~] = left_SVD_G(1);
        left_ASSIGN_G(U,1);
        
        [~,~,V] = right_SVD_G(4);
        right_ASSIGN_G(4,V');
        
        [U,~,~] = left_SVD_G(5);
        left_ASSIGN_G(U,5);
        
        [~,~,V] = right_SVD_G(6);
        Sigma = diag(1.5.^(-1:-1:-2*k));
        right_ASSIGN_G(6,Sigma*V');
        
        G{2}(:) = 0;
        G{3}(:) = 0;
        
        for i = 1:k
            G{2}(i,i,:) = 1;
            G{3}(i,i,:) = 1;
        end
        
        M_P = get_M_(P);
        M_P2 = get_M_(P2);
        M_C = get_M_(C);
    end

    function init()
        for s = 1:d
            G{s} = rand(r(s),r(s+1),n(s))-0.5;
            G{s} = G{s} / norm(G{s}(:));
        end
    end

    function [U,Sigma,V] = left_SVD_G(s) % get SVD of \mathfrac{L}(G_s)
        [U,Sigma,V] = svd(reshape(permute(G{s}(1:r(s),1:r(s+1),:),[1,3,2]),[r(s)*n(s),r(s+1)]),'econ');
    end

    function [U,Sigma,V] = right_SVD_G(s) % get SVD of \mathfrac{R}(G_s)
        [U,Sigma,V] = svd(reshape(G{s}(1:r(s),1:r(s+1),:),[r(s),r(s+1)*n(s)]),'econ');
    end

    function left_ASSIGN_G(U,s) % \mathfrac{L}(G_s) <- \mathfrac{L}(U)
        G{s}(1:r(s),1:r(s+1),:) = permute(reshape(U,[r(s),n(s),r(s+1)]),[1,3,2]);
    end

    function right_ASSIGN_G(s,Vt) % \mathfrac{R}(G_s) <- \mathfrac{R}(Vt)
        G{s}(1:r(s),1:r(s+1),:) = reshape(Vt,[r(s),r(s+1),n(s)]);
    end

    function M = get_M_(P)
        M = zeros(size(P,1),1);
        for i = 1:size(P,1)
            H = 1;
            for s = 1:d
                H = H * G{s}(:,:,P(i,s));
            end
            M(i) = H;
        end
    end
end
