function [M_P,M_P2,M_C,r,Sigma] = init_by_random_tensor(trial,para,P,P2,C)
% INIT_BY_RANDOM_TENSOR generates a "random" tensor with singular values 
% "uniformly distributed" in [0,1]
%
%   INIT_BY_RANDOM_TENSOR(trial,para,P,P2,C)
%   first finds a rank r s.t. mean(r) < para.r_rnd/3*2 && max(r) <
%   para.r_rnd, then initializes a random representation (uniformly in each
%   entry of G). Then it enforces uniformly distributed singular values by 
%   successive assignements of such.
%
%   The random seed is set to the value of trial.
%
%   Author:
%
%   Written by Sebastian Kraemer, IGPM RWTH Aachen University (2019)
%   In case of problems or questions, feel free to send an email to
%   kraemer@igpm.rwth-aachen.de
%
%   See the license file for copyright issues.

rng(trial);

d = para.d;
n = para.n;
r_rnd = para.r_rnd;
G = cell(1,d);

% find a suitable multivariate rank
r = ones(1,d+1);
while mean(r) < r_rnd/3*2 && max(r) < r_rnd
    r = max([r;[1,ceil(r_rnd*rand(1,d-1)),1]],[],1);
end
h_crt = 1;
[Sigma_,Sigma] = deal(cell(1,d+1));

main();

    function main()  
        init(); % initialize random representation
        randomize_sigma(); % randomize singular spectrum
        center_to(d,'record');
        for i = 1:20 % assign singular values
            center_to(1,'adapt');
            center_to(d,'adapt');
        end
        center_to(1,'record');
        
        % evaluate on subsets
        M_P = get_M_(P);
        M_P2 = get_M_(P2);
        M_C = get_M_(C);    
    end

    function randomize_sigma()
        for s = 2:d
            Sigma_{s} = diag(sort(rand(r(s),1),'descend'));
            Sigma_{s} = Sigma_{s}/norm(diag(Sigma_{s}));
        end
    end

    function init()       
        for s = 1:d
            G{s} = rand(r(s),r(s+1),n(s))-0.5;
            G{s} = G{s} / norm(G{s}(:));
        end       
    end

    function move_left(h_start,h_end,task) % left-orthogonalize left blocks
        for s = h_start:h_end-1
            [U,Sigma{s+1},V] = left_SVD_G(s);
            if strcmp(task,'adapt')
                Sigma{s+1} = Sigma_{s+1};
            end
            
            left_ASSIGN_G(U,s);
            left_MUL_G(Sigma{s+1}*V',s+1);
            
            h_crt = s+1;
        end
    end

    function move_right(h_start,h_end,task) % right-orthogonalize right blocks
        for s = h_start:-1:h_end+1
            [U,Sigma{s},V] = right_SVD_G(s);
            if strcmp(task,'adapt')
                Sigma{s} = Sigma_{s};
            end
            
            right_ASSIGN_G(s,V');
            right_MUL_G(s-1,U*Sigma{s});
            
            h_crt = s-1;
        end
    end

    function center_to(h_end,task) % orthogonalize with respect to h_end if orthogonal with respect to h_crt
        if (h_crt < h_end)
            move_left(h_crt,h_end,task);
        elseif (h_end < h_crt)
            move_right(h_crt,h_end,task);
        end
    end

    function [U,Sigma,V] = left_SVD_G(s) % get SVD of \mathfrac{L}(G_s)
        [U,Sigma,V] = svd(reshape(permute(G{s}(1:r(s),1:r(s+1),:),[1,3,2]),[r(s)*n(s),r(s+1)]),'econ');
    end

    function [U,Sigma,V] = right_SVD_G(s) % get SVD of \mathfrac{R}(G_s)
        [U,Sigma,V] = svd(reshape(G{s}(1:r(s),1:r(s+1),:),[r(s),r(s+1)*n(s)]),'econ');
    end

    function left_MUL_G(R,s) % R*G_s
        G{s}(1:r(s),1:r(s+1),:) =  reshape(R * reshape(...
            G{s}(1:r(s),1:r(s+1),:),[r(s),r(s+1)*n(s)]),...
            [r(s),r(s+1),n(s)]);
    end

    function right_MUL_G(s,L) % G_s*L
        G{s}(1:r(s),1:r(s+1),:) = permute(reshape(reshape(permute(...
            G{s}(1:r(s),1:r(s+1),:),[1,3,2]),...
            [r(s)*n(s),r(s+1)])*L,...
            [r(s),n(s),r(s+1)]),...
            [1,3,2]);
    end

    function left_ASSIGN_G(U,s) % \mathfrac{L}(G_s) <- \mathfrac{L}(U)
        G{s}(1:r(s),1:r(s+1),:) = permute(reshape(U,[r(s),n(s),r(s+1)]),[1,3,2]);
    end

    function right_ASSIGN_G(s,Vt) % \mathfrac{R}(G_s) <- \mathfrac{R}(Vt)
        G{s}(1:r(s),1:r(s+1),:) = reshape(Vt,[r(s),r(s+1),n(s)]);
    end

    function M = get_M_(P)
        M = zeros(size(P,1),1);
        for i = 1:size(P,1)
            H = 1;
            for s = 1:d
                H = H * G{s}(:,:,P(i,s));
            end
            M(i) = H;
        end
    end
end





